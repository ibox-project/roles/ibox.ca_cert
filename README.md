ca_cert
=========

Import CA certificates.

Requirements
------------

None.

Role Variables
--------------

| Variable                       | Required | Description                                    | Default |
|--------------------------------|----------|------------------------------------------------|---------|
| ca_cert__internal_ca_urls      | true     | Url of the cert to import                      | []      |
| ca_cert__validate_ca_cert_urls | true     | Validating certs for ca_cert__internal_ca_urls | []      |
| ca_cert__macos_keychain        | true     | Default keychain on MacOS                      | user    |

Dependencies
------------

None.

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.ca_cert.git
    version: stable
    scm: git
    name: ibox.ca_cert
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.ca_cert"
      ansible.builtin.include_role:
        name: "ibox.ca_cert"
      vars:
        ca_certs__ca_url: 'http://example.org/ca.crt'

```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
